
// basic element describing dom
const element = {
    type: "div",
    props: {
        id: "container",
        children: [
            { type: "input", props: { value: "foo", type: "text" } },
            { type: "a", props: { href: "/bar" } },
            { type: "span", props: {
              children: [
                { type: 'TEXT ELEMENT', props: { nodeValue: 'bar' } },
              ]}
            }
        ]
    }
};

// basic render function
function render(element, parentDom) {
    const { type, props } = element;
    const dom = document.createElement(type);
    const childElements = props.children || [];
    childElements.forEach(childElement => render(childElement, dom));
    parentDom.appendChild(dom);
}

// render function with events, properties, and text nodes
function render(element, parentDom) {
    const { type, props } = element;

    // create the dom element
    const dom = type === 'TEXT ELEMENT'
        ? document.createTextNode('')
        : document.createElement(type);
  
    // add event listeners
    const isListener = name => name.startsWith("on");
    Object.keys(props).filter(isListener).forEach(name => {
      const eventType = name.toLowerCase().substring(2);
      dom.addEventListener(eventType, props[name]);
    });
  
    // add properties
    const isAttribute = name => !isListener(name) && name != "children";
    Object.keys(props).filter(isAttribute).forEach(name => {
      dom[name] = props[name];
    });
  
    // render the children
    const childElements = props.children || [];
    childElements.forEach(childElement => render(childElement, dom));
  
    // append to parent
    parentDom.appendChild(dom);
}

render(element, document.getElementById('root'));