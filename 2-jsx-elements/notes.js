// Babel transplies JSX into createElement function calls
// turns this,
const element = (
  <div id="container">
    <input value="foo" type="text" />
    <a href="/bar">bar</a>
    <span onClick={e => alert("Hi")}>click me</span>
  </div>
);
// into this.
const element = createElement(
  "div",
  { id: "container" },
  createElement("input", { value: "foo", type: "text" }),
  createElement(
    "a",
    { href: "/bar" },
    "bar"
  ),
  createElement(
    "span",
    { onClick: e => alert("Hi") },
    "click me"
  )
);

// By default, babel uses React.createElement for transpiling.
// To tell babel to use a different createElement function, add 
// this comment, /** @jsx createElement */, to the top of the script.


/* * Defining createElement() * */

// createElement(type, config, ...args) {}
// type - the type of the element
// config - an object defining the elements props
// args - the elements children

// createElement needs to create a props object, 
// assign it all the values from the second argument, set the children property to an array 
// with all the arguments after the second, and then return an object with type and props. 

function createElement(type, config, ...args) {
    const props = Object.assign({}, config);
    const hasChildren = args.length > 0;
    props.children = hasChildren ? [].concat(...args) : [];
    return { type, props };
}

// The above definition does not taken into account text children, which are passed
// into createElement as strings. Each element needs to return an object with type and
// props. If each child is an Object, return it, if not, create a text element for the child.

const TEXT_ELEMENT = 'TEXT ELEMENT';

function createElement(type, config, ...args) {
    const props = Object.assign({}, config);
    const hasChildren = args.length > 0;
    const rawChildren = hasChildren ? [].concat(...args) : [];
    props.children = rawChildren
        .filter(c => c != null && c != false)
        .map(c => c instanceof Ojbect ? c : createTextElement(c));
    return { type, props };
}

function createTextElement(value) {
    return createElement(TEXT_ELEMENT, { nodeValue: value });
}