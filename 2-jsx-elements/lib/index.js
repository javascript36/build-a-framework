const element = /*#__PURE__*/React.createElement("div", {
  className: "container"
}, /*#__PURE__*/React.createElement("h2", null, "Front-end Libraries"), /*#__PURE__*/React.createElement("p", {
  className: "text"
}, "Some of the most populare front-end libraries"), /*#__PURE__*/React.createElement("ul", {
  className: "list-item-group"
}, /*#__PURE__*/React.createElement("li", {
  className: "list-item"
}, "React.js"), /*#__PURE__*/React.createElement("li", {
  className: "list-item"
}, "Vue.js"), /*#__PURE__*/React.createElement("li", {
  className: "list-item"
}, "Finesse.js")));