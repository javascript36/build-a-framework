
const element = (
    <div className="container">
        <h2>Front-end Libraries</h2>
        <p className="text">Some of the most populare front-end libraries</p>
        <ul className="list-item-group">
            <li className="list-item">React.js</li>
            <li className="list-item">Vue.js</li>
            <li className="list-item">Finesse.js</li>
        </ul>
    </div>
);