const LINE_BREAK = { type: "br", props: {} };

// element describing the dom
const element = {
  type: "div",
  props: {
    id: "container",
    children: [
      "",
      null,
      { type: "input", props: { value: "foo", type: "text" } },
      LINE_BREAK,
      { type: "a", props: { href: "/bar" } },
      LINE_BREAK,
      {
        type: "span",
        props: {
          children: [{ type: "TEXT ELEMENT", props: { nodeValue: "bar" } }],
        },
      },
    ],
  },
};

// render function that observes events, properties, and text nodes
function render(element, parentDom, replaceDom = false) {
  
  const { type, props } = element;

  // filtering callbacks
  const notNullOrFalse = (value) => value != null && value != false;
  const isListener = (value) => value.startsWith("on");
  const isAttribute = (value) => !isListener(value) && value != "children";

  // create the dom node
  const dom =
    type === "TEXT ELEMENT"
      ? document.createTextNode("")
      : document.createElement(type);

  // add event listener props
  Object.keys(props)
    .filter(isListener)
    .forEach((name) => {
      const eventType = name.toLowerCase().substring(2);
      dom.addEventListener(eventType, props[name]);
    });

  // add properties
  Object.keys(props)
    .filter(isAttribute)
    .forEach((name) => {
      dom[name] = props[name];
    });

  // render the children
  const rawChildren = props.children || [];
  rawChildren
    .filter(notNullOrFalse)
    .forEach((childElement) => render(childElement, dom));

  // Append or replace dom
  if (replaceDom) {
    parentDom.replaceChild(dom, parentDom.lastChild);
  } else {
    parentDom.appendChild(dom);
  }
}

// render to the real dom
render(element, document.getElementById("root"));

// produce a re-rendering of dom
setTimeout(function () {
  const newElement = {
    type: "div",
    props: {
      id: "container",
      children: [
        { type: "input", props: { value: "foo replacement", type: "text" } },
        LINE_BREAK,
        {
          type: "a",
          props: {
            href: "/www.google.com",
            target: "_blank",
            children: [
              { type: "TEXT ELEMENT", props: { nodeValue: "Google.com" } },
            ],
          },
        },
        LINE_BREAK,
        {
          type: "span",
          props: {
            children: [
              { type: "TEXT ELEMENT", props: { nodeValue: "bar replacement" } },
            ],
          },
        },
      ],
    },
  };
  render(newElement, document.getElementById("root"), true);
}, 2000);
